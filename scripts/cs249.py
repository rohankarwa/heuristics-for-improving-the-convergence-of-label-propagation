from subprocess import call
from sys import argv

script, input_file, output_base_directory, local_output_base_directory = argv

def my_copy(src_location, dest_location):
	src = open(src_location)
	dest = open(dest_location, 'w')
	src_content = src.read()
	dest.write(src_content)
	src.close()
	dest.close()


lines = [line.strip() for line in open(input_file)]
counter = 1
for line in lines:
	files = line.split("\t")
	filepath = files[0]
	print "Doing Analysis for file", filepath
	output_directory = output_base_directory + "/" + str(counter)
	local_output_directory = local_output_base_directory + "/output" + str(counter) 
	call(["../bin/hadoop", "jar", "../mycode/feb27/mycode/map-reduce.jar", "com.community.rohan.mapreduce.MyMapReduce", filepath, output_directory, local_output_directory])
	bm_community_file = files[1]
	dest_location = local_output_directory + "/community_bm.txt"
	print "Copying %s to %s" % (bm_community_file, dest_location)
	my_copy(bm_community_file, dest_location)
	map_red_communities = local_output_directory + "/communities.txt"
	analysis_file = local_output_directory + "/analysis.txt"
	call(["java","-jar","result-analysis.jar",map_red_communities,dest_location,analysis_file])
	counter = counter + 1



