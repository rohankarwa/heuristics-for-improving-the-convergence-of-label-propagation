package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the Bean which holds the result for a specific community 
 * comparison
 * @author Rohan Karwa
 *
 */
public class AnalysisResult {

	private List<Integer> allBMNodes = new ArrayList<Integer>();
	private List<Integer> allMapRedCommunityNodes = new ArrayList<Integer>();
	private List<Integer> matchedNodeIds = new ArrayList<Integer>();
	private List<Integer> bmNodesNotPresent = new ArrayList<Integer>();
	private List<Integer> mapRedNodesNotPresent = new ArrayList<Integer>();
	public String getContentToPersist() {
		String output = listToString(allBMNodes) + "\t" +
						listToString(allMapRedCommunityNodes) + "\t" +
						listToString(bmNodesNotPresent) + "\t" +
						listToString(mapRedNodesNotPresent) + "\t" +
						getFPRate() + "\t" + 
						getFNRate();
						
		return output;
	}
	
	private String getFNRate() {
		Double rate = (double)bmNodesNotPresent.size() / getMaxNodes(); 
		return "False Negative: " + rate + ", Nodes = " + bmNodesNotPresent.size();
	}
	
	public boolean isFNPresent() {
		if(bmNodesNotPresent.size() == 0) return false;
		return true;
	}
	
	public boolean isFPPresent() {
		if(mapRedNodesNotPresent.size() == 0) return false;
		return true;
	}

	private String getFPRate() {
		Double rate = (double)mapRedNodesNotPresent.size() / getMaxNodes(); 
		return "False Positive: " + rate + ", Nodes = " + mapRedNodesNotPresent.size();
	}
	
	private int getMaxNodes() {
		if(allBMNodes.size() > allMapRedCommunityNodes.size()) return allBMNodes.size();
		else return allMapRedCommunityNodes.size();
	}

	public void addMatchedNodeId(int nodeId) {
		matchedNodeIds.add(nodeId);
	}
	
	public void addBMNodeNotPresent(int nodeId) {
		bmNodesNotPresent.add(nodeId);
	}
	
	public void addMapRedNodesNotPresent(int nodeId) {
		mapRedNodesNotPresent.add(nodeId);
	}

	public void setAllBMNodes(List<Integer> list) {
		allBMNodes = list;
	}
	
	public void setAllMapRedCommunityNodes(List<Integer> list) {
		allMapRedCommunityNodes = list;
	}
	
	private String listToString(List<Integer> list) {
		StringBuffer stringBuffer = new StringBuffer();
		for(int nodeId : list) {
			stringBuffer.append(nodeId + ",");
		}
		String output = stringBuffer.toString();
		if(output.length()>1)
			output = output.substring(0, output.length() - 1);
		else output = "NOTHING";
		return output;
	}
	
	public int getWronglyClassifiedNodesFP() {
		return mapRedNodesNotPresent.size();
	}
	
	public int getWronglyClassifiedNodesFN() {
		return bmNodesNotPresent.size();
	}
}
