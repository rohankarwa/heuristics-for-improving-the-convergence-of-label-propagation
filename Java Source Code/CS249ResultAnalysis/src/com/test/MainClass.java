package com.test;

/**
 * Entry point
 * @author hadoop
 *
 */
public class MainClass {

	public static void main(String args[]) {
		String mapRedFile = args[0];
		String benchMarkFile = args[1];
		String outputFile = args[2];
	
//		String mapRedFile = "/home/hadoop/hadoop/data/output_march9_2/output1/communities.txt";
//		String benchMarkFile = "/home/hadoop/hadoop/data/output_march9_2/output1/community_bm.txt";
//		String outputFile = "/home/hadoop/hadoop/data/output_march9_2/output1/analysis_tmp1.txt";
//	
		
		CommunityComparerNew communityComparer = new CommunityComparerNew(mapRedFile, benchMarkFile, outputFile);
		communityComparer.analyzeCommunities();
	}
}
