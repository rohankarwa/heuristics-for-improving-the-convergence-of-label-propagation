package com.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class handles all the File Reading operations
 * @author Rohan Karwa
 *
 */
public class MyFileReader {

	private String fileName;
	private BufferedReader bufferReader = null;
	public MyFileReader(String fileName) {
		this.fileName = fileName;
	}
	
	public String readNextLine() {
		if(bufferReader == null) initializeBufferedReader();
		try {
			return bufferReader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	private void initializeBufferedReader() {
		if(bufferReader != null) return;
		try {
			bufferReader = new BufferedReader(new FileReader(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeFile() {
		try {
			bufferReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
