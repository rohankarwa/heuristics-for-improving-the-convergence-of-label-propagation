package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * This class maintains the summary 
 * @author Rohan Karwa
 *
 */
public class ResultSummary {

	private List<String> falseNegativeSummary = new ArrayList<String>();
	private List<String> falsePositiveSummary = new ArrayList<String>();
	private List<String> bothFPAndFNSummary = new ArrayList<String>();
	private int perfectMatch = 0;
	private int totalWrongClassifiedFP = 0;
	private int totalWrongClassifiedFN = 0;
	
	public void extractInfoForFinalReport(AnalysisResult analysisResult) {
		totalWrongClassifiedFP += analysisResult.getWronglyClassifiedNodesFP();
		totalWrongClassifiedFN += analysisResult.getWronglyClassifiedNodesFN();
		String resultContent = analysisResult.getContentToPersist();
		if(analysisResult.isFNPresent() && analysisResult.isFPPresent()) {
			bothFPAndFNSummary.add(resultContent);
		} else if(analysisResult.isFNPresent()) {
			falseNegativeSummary.add(resultContent);
		} else if(analysisResult.isFPPresent()) {
			falsePositiveSummary.add(resultContent);
		} else {
			perfectMatch += 1;
		}
	}
	
	public String getSummaryContent() {
		
		String stars = "\n*********************************************\n";
		String bothFPFNSummary = getBothFPFNSummary();
		String fpSummary = getFPSummary();
		String fnSummary = getFNSummary(); 
		String output = stars + "\nFinal Summary" +  
				stars + "\n" + "Perfect Community Matches = " + perfectMatch +
				"\nTotal Wrongly Classified Nodes FP= " + totalWrongClassifiedFP +
				"\nTotal Wrongly Classified Nodes FN= " + totalWrongClassifiedFN +
				stars + "\n" + bothFPFNSummary +
				stars + "\n" + fpSummary + 
				stars + "\n" + fnSummary;
				
		return output;		
	}

	private String getBothFPFNSummary() {
		StringBuffer output = new StringBuffer();
		output.append("\nCommunities having both FP and FN: " + bothFPAndFNSummary.size());
		for(String value : bothFPAndFNSummary) {
			output.append("\n" + value);
		}
		return output.toString();
	}
	
	private String getFPSummary() {
		StringBuffer output = new StringBuffer();
		output.append("\nCommunities having only FP: " + falsePositiveSummary.size());
		for(String value : falsePositiveSummary) {
			output.append("\n" + value);
		}
		return output.toString();
	}
	
	private String getFNSummary() {
		StringBuffer output = new StringBuffer();
		output.append("\nCommunities having only FN: " + falseNegativeSummary.size());
		for(String value : falseNegativeSummary) {
			output.append("\n" + value);
		}
		return output.toString();
	}
}
