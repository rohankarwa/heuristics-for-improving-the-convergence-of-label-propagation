package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Rohan Karwa
 *
 */
public class Community {

	private List<Integer> nodeIds = new ArrayList<Integer>();
	
	public void addNode(int nodeId) {
		nodeIds.add(nodeId);
	}
	
	public List<Integer> getNodeIds(){
		return nodeIds;
	}
	
	public void addList(String allNodeIds) {
		String[] splittedNodeIds = allNodeIds.split(",");
		for(String nodeId : splittedNodeIds){
			nodeIds.add(Integer.parseInt(nodeId.trim()));
		}
	}
}
