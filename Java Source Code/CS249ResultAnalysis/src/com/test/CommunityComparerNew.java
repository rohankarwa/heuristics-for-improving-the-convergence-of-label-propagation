package com.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Rohan Karwa
 *
 */
public class CommunityComparerNew {

	private String mapRedFile;
	private String benchMarkFile;
	private String outputFile;
	private MyFileReader mapRedFileReader = null;
	private MyFileWriter fileWriter = null;
	
	private Map<Integer, Integer> bmNodeLabelMap = new HashMap<Integer, Integer>();
	private Map<Integer, List<Integer>> bmLabelNodesMap = new HashMap<Integer, List<Integer>>();
	public CommunityComparerNew(String mapRedFile, String benchMarkFile, String outputFile) {
		this.mapRedFile = mapRedFile;
		this.benchMarkFile = benchMarkFile;
		this.outputFile = outputFile;
	}
	
	public void analyzeCommunities() {
		initializeBMMaps();
		ResultSummary resultSummary = new ResultSummary();
		List<Community> allMapRedCommunities = new ArrayList<Community>();
		Community community = getNextMapRedCommunity();
		while(community != null) {
			allMapRedCommunities.add(community);
			community = getNextMapRedCommunity();
		}
		System.out.println("Start = " + allMapRedCommunities.size());
		for(List<Integer> bmCommunity : bmLabelNodesMap.values()) {
			AnalysisResult analysisResult = compare(bmCommunity, allMapRedCommunities);
			String contentToWrite = analysisResult.getContentToPersist();
			writeToOutputFile(contentToWrite);
			resultSummary.extractInfoForFinalReport(analysisResult);
		}
		
		//By now all the map-reduce community should be exhausted
		//if any is present then they are False postive ones
		System.out.println("Remaining = " + allMapRedCommunities.size());
		for(Community mapRedRemainingCommunity: allMapRedCommunities) {
			AnalysisResult analysisResult = new AnalysisResult();
			analysisResult.setAllMapRedCommunityNodes(mapRedRemainingCommunity.getNodeIds());
			for(int nodeId: mapRedRemainingCommunity.getNodeIds())
				analysisResult.addMapRedNodesNotPresent(nodeId);
			String contentToWrite = analysisResult.getContentToPersist();
			writeToOutputFile(contentToWrite);
			resultSummary.extractInfoForFinalReport(analysisResult);
		}
		
//		ResultSummary resultSummary = new ResultSummary();
//		Community community = getNextMapRedCommunity();
//		while(community != null) {
//			AnalysisResult analysisResult = analyzeCommunity(community);
//			String contentToWrite = analysisResult.getContentToPersist();
//			writeToOutputFile(contentToWrite);
//			resultSummary.extractInfoForFinalReport(analysisResult);
//			community = getNextMapRedCommunity();
//		}
		writeToOutputFile(resultSummary.getSummaryContent());
		closeMapRedReader();
		closeWriterFile();
	}
	
	private AnalysisResult compare(List<Integer> bmCommunity,
			List<Community> allMapRedCommunities) {
		int maxCommunityIndex = -1;
		int maxHit = -1;

		for(int index = 0; index < allMapRedCommunities.size(); index++) {
			Community mapRedCommunity = allMapRedCommunities.get(index);
			int numberOfCommonNodes = getCommonNodes(bmCommunity, mapRedCommunity);
			if(numberOfCommonNodes > maxHit) {
				maxHit = numberOfCommonNodes;
				maxCommunityIndex = index;
			}
		}
		
		if(maxCommunityIndex == -1) {
			AnalysisResult analysisResult = new AnalysisResult();
			analysisResult.setAllBMNodes(bmCommunity);
			analysisResult.setAllMapRedCommunityNodes(new ArrayList<Integer>());
			for(int nodeId: bmCommunity)
				analysisResult.addBMNodeNotPresent(nodeId);
			return analysisResult;
		}
		
		Community community = allMapRedCommunities.get(maxCommunityIndex);
		allMapRedCommunities.remove(maxCommunityIndex);
		
		AnalysisResult analysisResult = new AnalysisResult();
		analysisResult.setAllMapRedCommunityNodes(community.getNodeIds());
				
		analysisResult.setAllBMNodes(bmCommunity);
		
		for(int benchMarkNode : bmCommunity) {
			if(community.getNodeIds().contains(benchMarkNode)) {
				//Matched
				analysisResult.addMatchedNodeId(benchMarkNode);
			} else {
				//Extra Node in BM and not present in the predicted community
				analysisResult.addBMNodeNotPresent(benchMarkNode);
			}
		}
		
		for(int nodeIdOfMapRedCommunity : community.getNodeIds()) {
			if(!bmCommunity.contains(nodeIdOfMapRedCommunity)) {
				analysisResult.addMapRedNodesNotPresent(nodeIdOfMapRedCommunity);
			}
		}
		
		return analysisResult;
		
	}

	private int getCommonNodes(List<Integer> bmCommunity,
			Community mapRedCommunity) {
		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
		for(int bmCommunityNode : bmCommunity) {
			map.put(bmCommunityNode, false);
		}
		int count = 0;
		for(int mapRedCommunityNode : mapRedCommunity.getNodeIds()) {
			if(map.containsKey(mapRedCommunityNode)) count++;
		}
		return count;
	}

	private AnalysisResult analyzeCommunity(Community community) {
		Map<Integer, Integer> nodeIdCommunityId = new HashMap<Integer, Integer>();
		//Make a note of labels to which nodes from Map-Red Belong
		for(int nodeId : community.getNodeIds()) {
			int communityIdFromBM = bmNodeLabelMap.get(nodeId);
			nodeIdCommunityId.put(nodeId, communityIdFromBM);
		}
		
		//Count the labels for Map-Red nodeIds
		Map<Integer, Integer> communityIdCount = new HashMap<Integer, Integer>();
		for(int nodeId : nodeIdCommunityId.keySet()) {
			int communityId = nodeIdCommunityId.get(nodeId); 
			if(communityIdCount.containsKey(communityId)) {
				communityIdCount.put(communityId, communityIdCount.get(communityId) + 1);
			} else {
				communityIdCount.put(communityId, 1);
			}
		}
		
		//Look for MAX community
		int maxCommunityId = -1;
		int maxCount = -1;
		for(int communityId : communityIdCount.keySet()) {
			int count = communityIdCount.get(communityId);
			if(count > maxCount) {
				maxCount = count;
				maxCommunityId = communityId;
			}
		}
		
		AnalysisResult analysisResult = new AnalysisResult();
		analysisResult.setAllMapRedCommunityNodes(community.getNodeIds());
		
		//NO_MAX
		if((maxCount < community.getNodeIds().size()/2) ||
				maxCount < bmLabelNodesMap.get(maxCommunityId).size()/2) {
			for(int nodeId : community.getNodeIds())
				analysisResult.addMapRedNodesNotPresent(nodeId);
			return analysisResult;
		}
		
		analysisResult.setAllBMNodes(bmLabelNodesMap.get(maxCommunityId));
		
		for(int benchMarkNode : bmLabelNodesMap.get(maxCommunityId)) {
			if(nodeIdCommunityId.containsKey(benchMarkNode)) {
				//Matched
				analysisResult.addMatchedNodeId(benchMarkNode);
			} else {
				//Extra Node in BM and not present in the predicted community
				analysisResult.addBMNodeNotPresent(benchMarkNode);
			}
		}
		
		for(int nodeIdOfMapRedCommunity : nodeIdCommunityId.keySet()) {
			int communityId = nodeIdCommunityId.get(nodeIdOfMapRedCommunity);
			if(communityId != maxCommunityId) {
				analysisResult.addMapRedNodesNotPresent(nodeIdOfMapRedCommunity);
			}
		}
		
		return analysisResult;
	}

	private void initializeBMMaps() {
		MyFileReader myFileReader = new MyFileReader(benchMarkFile);
		String data = myFileReader.readNextLine();
		while(data != null && !data.equalsIgnoreCase("")) {
			String[] splittedData = data.split("\t");
			if(splittedData.length != 2) {
				System.out.println("Something is wrong with the benchmark file");
			} else {
				int nodeId = Integer.parseInt(splittedData[0].trim());
				int communityId = Integer.parseInt(splittedData[1].trim());
				bmNodeLabelMap.put(nodeId, communityId);
				if(bmLabelNodesMap.containsKey(communityId)) {
					bmLabelNodesMap.get(communityId).add(nodeId);
				} else {
					List<Integer> newList = new ArrayList<Integer>();
					newList.add(nodeId);
					bmLabelNodesMap.put(communityId, newList);
				}
			}
			data = myFileReader.readNextLine();
		}
		myFileReader.closeFile();
	}
	
	private Community getNextMapRedCommunity() {
		if(mapRedFileReader == null) initializeMapRedReader();
		String communityList = mapRedFileReader.readNextLine();
		if(communityList == null || communityList.equalsIgnoreCase("")) return null;
		Community community = new Community();
		community.addList(communityList);
		return community;
	}
	
	private void initializeMapRedReader() {
		mapRedFileReader = new MyFileReader(mapRedFile);
	}
	
	private void initializeFileWriter() {
		fileWriter = new MyFileWriter(outputFile);
	}
	
	private void writeToOutputFile(String content) {
		if(fileWriter == null) initializeFileWriter();
		fileWriter.writeLine(content);
	}
	
	private void closeWriterFile(){
		fileWriter.close();
	}
	
	private void closeMapRedReader() {
		mapRedFileReader.closeFile();
	}
}