package com.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class manages all the File Write Operations
 * @author Rohan Karwa
 *
 */
public class MyFileWriter {

	private String fileName;
	private BufferedWriter bufferedWriter = null;
	public MyFileWriter(String fileName) {
		this.fileName = fileName;
	}
	
	public void writeLine(String content) {
		if(bufferedWriter == null) initializeBufferedWriter();
		try {
			bufferedWriter.write("\n" + content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initializeBufferedWriter() {
		if(bufferedWriter != null) return;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
