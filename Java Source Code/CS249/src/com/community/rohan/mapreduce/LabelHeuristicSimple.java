package com.community.rohan.mapreduce;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is the Simple LP. It doesn't assume any T or Window W
 * @author Rohan Karwa
 *
 */
public class LabelHeuristicSimple extends LabelHeuristic{


	private String generateLabel(String labels, String badLabels) {		
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		String[] allLabels = labels.split(",");
		for(String currentLabel : allLabels) {
			//String nodeId = currentLabel.substring(1, currentLabel.indexOf(":"));
			String nodeLabel = currentLabel.substring(currentLabel.indexOf(":") + 1, currentLabel.length() - 1);
			if(countMap.containsKey(nodeLabel)) 
				countMap.put(nodeLabel, countMap.get(nodeLabel) + 1);
			else
				countMap.put(nodeLabel, 1);
		}
		
		String maxLabel = "";
		List<String> candidateLabels = new ArrayList<String>();
		int maxValue = -1;
		for(String nodeLabel : countMap.keySet()) {
			if(countMap.get(nodeLabel) > maxValue) {
				maxLabel = nodeLabel;
				maxValue = countMap.get(nodeLabel);
				candidateLabels = new ArrayList<String>();
				candidateLabels.add(maxLabel);
			} else if(countMap.get(nodeLabel) == maxValue) {
				candidateLabels.add(nodeLabel);
			}
		}
		
		if(maxLabel.equalsIgnoreCase("")) return maxLabel;
		if(candidateLabels.size() == 1) return maxLabel;
		
		int randomNumber = (int)(Math.random() * candidateLabels.size());
		return candidateLabels.get(randomNumber);
	}


	public boolean skipTheNode(String labels) {
		//never
		return false;
	}

	@Override
	public String labelHistoryToPreserve(String currentLabel, String olderLabels) {
		//All History
		return currentLabel + "," + olderLabels;
	}

	@Override
	public String generateLabel(String labels, String badLabels,
			String lastCurrentNodeLabel, int iterationNumber) {
		
		return generateLabel(labels, badLabels);
	}
	
	@Override
	public boolean skipTheNode(String labels, long counter) {
		return skipTheNode(labels);
	}

}
