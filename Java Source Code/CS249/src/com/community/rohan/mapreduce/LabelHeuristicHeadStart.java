package com.community.rohan.mapreduce;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Rohan Karwa
 *
 */
public class LabelHeuristicHeadStart extends LabelHeuristic {

	
	private String generateLabel(String labels, String badLabels, String lastCurrentNodeLabel) {
		
		List<Integer> listOfBadLabels = new ArrayList<Integer>();//generateList(badLabels);
		
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		String[] allLabels = labels.split(",");
		for(String currentLabel : allLabels) {
			String nodeId = currentLabel.substring(1, currentLabel.indexOf(":"));
			if(listOfBadLabels.contains(Integer.parseInt(nodeId.trim()))) 
				continue;
			String nodeLabel = currentLabel.substring(currentLabel.indexOf(":") + 1, currentLabel.length() - 1);
			
			nodeLabel = headStart(nodeLabel, lastCurrentNodeLabel);
			
			if(countMap.containsKey(nodeLabel)) 
				countMap.put(nodeLabel, countMap.get(nodeLabel) + 1);
			else
				countMap.put(nodeLabel, 1);
		}
//		String maxLabel = "";
//		int maxValue = -1;
//		for(String nodeLabel : countMap.keySet()) {
//			if(countMap.get(nodeLabel) > maxValue) {
//				maxLabel = nodeLabel;
//				maxValue = countMap.get(nodeLabel);
//			}
//		}
		String maxLabel = "";
		List<String> candidateLabels = new ArrayList<String>();
		int maxValue = -1;
		for(String nodeLabel : countMap.keySet()) {
			if(countMap.get(nodeLabel) > maxValue) {
				maxLabel = nodeLabel;
				maxValue = countMap.get(nodeLabel);
				candidateLabels = new ArrayList<String>();
				candidateLabels.add(maxLabel);
			} else if(countMap.get(nodeLabel) == maxValue) {
				candidateLabels.add(nodeLabel);
			}
		}
		
		if(maxLabel.equalsIgnoreCase("")) return maxLabel;
		if(candidateLabels.size() == 1) return maxLabel;
		
		int randomNumber = (int)(Math.random() * candidateLabels.size());
		return candidateLabels.get(randomNumber);
	}

	private String headStart(String nodeLabel, String lastCurrentNodeLabel) {
		int randomNumber = (int)(Math.random() * 3);
		if(randomNumber == 2) return lastCurrentNodeLabel;
		else return nodeLabel;
	}

	private List<Integer> generateList(String badLabels) {
		String[] splits = badLabels.split(",");
		List<Integer> listToReturn = new ArrayList<Integer>();
		for(String nodeId : splits) {
			nodeId = nodeId.trim();
			if(nodeId.equalsIgnoreCase("")) continue;
			listToReturn.add(Integer.parseInt(nodeId));
		}
		return listToReturn;
	}


	public boolean skipTheNode(String labelList) {
//		String[] labels = labelList.split(",");
//		if(labels.length < 8) 
//			return false;
//		String firstLabel = "";
//		for(int i=0; i<3;i++) {
//			if(i == 0) {
//				firstLabel = labels[i];
//			}
//			else {
//				if(!firstLabel.equalsIgnoreCase(labels[i])) return true;
//			}
//		}
		return false;
	}

	@Override
	public String labelHistoryToPreserve(String currentLabel, String olderLabels) {
		return currentLabel + "," + olderLabels;
	}

	@Override
	public String generateLabel(String labels, String badLabels,
			String lastCurrentNodeLabel, int iterationNumber) {
		return generateLabel(labels, badLabels, lastCurrentNodeLabel);
	}

	@Override
	public boolean skipTheNode(String labels, long counter) {
		return skipTheNode(labels);
	}

}