package com.community.rohan.mapreduce;

/**
 * Helper class
 * @author Rohan Karwa
 *
 */
public class Utility {
	public static String trimLastCharacter(String string) {
		if(string.length() == 0) return string;
		return string.substring(0, string.length()-1);
	}

}
