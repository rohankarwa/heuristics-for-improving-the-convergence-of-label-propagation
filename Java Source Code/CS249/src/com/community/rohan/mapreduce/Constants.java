package com.community.rohan.mapreduce;

/**
 * Class mentioning all the constant values
 * @author Rohan Karwa
 *
 */
public class Constants {

	public static final String DO_NOT_CONSIDER_VOTE = "DO_NOT_CONSIDER_VOTE";
	public static final String LABEL_CHANGES = "LABEL_CHANGES";
	public static final String REALLY_BORDER_LABELS = "REALLY_BORDER_LABELS";
	public static final String NO_LABEL = "NO_LABEL";
	public static final String NO_VOTE = "NO_VOTE";
	

	
}
