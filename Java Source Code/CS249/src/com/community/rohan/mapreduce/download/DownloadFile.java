package com.community.rohan.mapreduce.download;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * This is class is responsible for handling all the operations with HDFS
 * @author Rohan Karwa
 *
 */
public class DownloadFile {

	/**
	 * Copies file from HDFS to the local file system
	 * @param hadoopFilePath
	 * @param localFilePath
	 */
	public static void copyFileToLocalSystem(String hadoopFilePath, String localFilePath) {
		Configuration config = new Configuration();
		try {
	   
	        FileSystem hdfs = FileSystem.get(config);	
	        System.out.println("Home Directory--> " + hdfs.getHomeDirectory().toString());
			Path inputPath = new Path(hadoopFilePath);
			Path outputPath = new Path(localFilePath);
			hdfs.copyToLocalFile(false, inputPath, outputPath);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
//	
//	public static void main(String args[]) {
//		copyFileToLocalSystem("/user/hadoop/data/input1.txt", "/home/hadoop/hadoop/tmp/test200.txt");
//	}
}
