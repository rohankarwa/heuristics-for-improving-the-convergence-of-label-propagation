package com.community.rohan.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.community.rohan.mapreduce.download.DownloadFile;

/**
 * For Mapper: 
 * 	Input: 
 * 		Key: NodeId 
 *  	Value: AdjNode1, AdjNode2
 *  Output: 
 *  	Key: AdjNode1 
 *  	Value: NodeId
 * 
 * For Reducer: 
 * 	Input: 
 * 		Key: NodeId 
 * 		Value: List<AdjNodes> 
 *	Output: 
 *		Key: NodeId
 * 		Value: GENERATED_LABEL|AdjNode1,AdjNode2 
 * & 	Key: NodeId 
 * 		Value: GENERATED_LABEL|NodeId|AdjNode1,AdjNode2 
 * 		The second key/value pair would be
 * 		useful in the book-keeping
 * 
 * @author Rohan Karwa
 * 
 */
public class MyMapReduce extends Configured implements Tool {

	public static class MapInitialClass extends MapReduceBase implements
			Mapper<Text, Text, Text, Text> {

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			String currentNode = key.toString();
			String adjList = value.toString();
			String[] adjNodes = adjList.split(",");
			for(String adjNode : adjNodes) {
				output.collect(new Text(adjNode), new Text(currentNode));
			}
		}
	}

	public static class ReduceInitialClass extends MapReduceBase implements
			Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			StringBuffer adjNodes = new StringBuffer();
			while (values.hasNext()) {
				String currentValue = values.next().toString();
				adjNodes.append("<" + currentValue + ":" + currentValue + ">,");
			}
			
			String allAdjNodes = adjNodes.toString();
			allAdjNodes = Utility.trimLastCharacter(allAdjNodes);
			
			LabelHeuristic labelGenerator = LabelHeuristic.getInstance();
			String generatedLabel = labelGenerator.generateLabel(allAdjNodes, "", key.toString(), 1);
			
			//Output Type1:
			String type1Value = generatedLabel + "|" + allAdjNodes;
			output.collect(key, new Text(type1Value));
			
			//Output Type2:
			String type2Value = generatedLabel + "|" + key.toString() + "|" + allAdjNodes;
			output.collect(key, new Text(type2Value));
		}
	}

	public static class MapPropagationClass extends MapReduceBase implements
			Mapper<Text, Text, Text, Text> {

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			String incomingValue = value.toString();
			long counter = reporter.getCounter(Constants.LABEL_CHANGES, Constants.LABEL_CHANGES).getCounter();
			
			if(isType1Value(incomingValue)) {
				String[] message = incomingValue.split("\\|");
				String[] adjNodes = message[1].split(",");
				String nodeLabel = message[0];
				for(String adjNodeMessage : adjNodes) {
					//<adjNode:label>
					String adjNode = adjNodeMessage.substring(1, adjNodeMessage.indexOf(":"));
					output.collect(new Text(adjNode), new Text(nodeLabel + "|" + 
															key.toString()));
				}
			} else {
				//We need to pass the same message anyway, so doing it first
				//Type2Message
				output.collect(key, value);
				
				String[] message = incomingValue.split("\\|");
				String label = message[0];
				LabelHeuristic labelHeuristic = LabelHeuristic.getInstance();
				if(labelHeuristic.skipTheNode(label, counter)) {
					//Heuristics is saying for not considering the vote
					//for this label. So send skipping information
					String[] adjNodes = message[2].split(",");
					for(String adjNodeMessage : adjNodes) {
						//<adjNode:label>
						String adjNode = adjNodeMessage.substring(1, adjNodeMessage.indexOf(":"));
						output.collect(new Text(adjNode), new Text(key.toString()+ "|" + 
																Constants.DO_NOT_CONSIDER_VOTE));
					}
				}
			}
		}

		private boolean isType1Value(String incomingValue) {
			if(incomingValue.split("\\|").length == 2)	return true;
			return false;
		}
	}

	public static class ReducePropagationClass extends MapReduceBase implements
	Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			StringBuffer adjLabelsBuffer = new StringBuffer();
			StringBuffer badNodesBuffer = new StringBuffer();
			String type2message = "";
			while (values.hasNext()) {
				String currentValue = values.next().toString();
				
				if(isBadLabelMessage(currentValue)) {
					badNodesBuffer.append(currentValue.split("\\|")[0] + ",");
				} else if(isType2Message(currentValue)) {
					type2message = currentValue;
				} else {
					//Type 1 message
					String[] message = currentValue.split("\\|");
					String label = message[0];
					String nodeId = message[1];
					adjLabelsBuffer.append("<" + nodeId + ":" + label + ">,");
				}
			}
			
			String badNodes = Utility.trimLastCharacter(badNodesBuffer.toString());
			String adjLabels = Utility.trimLastCharacter(adjLabelsBuffer.toString());
			
			//Output Type2:
			String[] type2messagePart = type2message.split("\\|"); 
			String olderLabels = type2messagePart[0];
			//Tracking the label Changes
			String lastLabel = olderLabels.split(",")[0];
			int iterationNumber = olderLabels.split(",").length;
			
			LabelHeuristic labelGenerator = LabelHeuristic.getInstance();
			String generatedLabel = labelGenerator.generateLabel(adjLabels, badNodes, lastLabel, iterationNumber);
		
			if(generatedLabel.equalsIgnoreCase(Constants.NO_LABEL)) {
				reporter.incrCounter(Constants.REALLY_BORDER_LABELS, Constants.REALLY_BORDER_LABELS, 1);
			}
			
			if(generatedLabel.equalsIgnoreCase("")) {
				generatedLabel = lastLabel;
				reporter.incrCounter(Constants.NO_VOTE, Constants.NO_VOTE, 1);
			}
			
			if(!generatedLabel.equalsIgnoreCase(lastLabel)) {
				//Increment the counter. Ie label is changed
				reporter.incrCounter(Constants.LABEL_CHANGES, Constants.LABEL_CHANGES, 1);
			}
			
			String labelHistory = labelGenerator.labelHistoryToPreserve(generatedLabel, olderLabels);
			String type2Value = labelHistory + "|" + type2messagePart[1] + "|" + adjLabels;
			output.collect(key, new Text(type2Value));
			
			//Output Type1:
			String type1Value = generatedLabel + "|" + adjLabels;
			output.collect(key, new Text(type1Value));
			

		}

		private boolean isBadLabelMessage(String currentValue) {
			if(currentValue.split("\\|").length ==2 && currentValue.split("\\|")[1].equalsIgnoreCase(Constants.DO_NOT_CONSIDER_VOTE))
				return true;
			return false;
		}

		private boolean isType2Message(String currentValue) {
			if(currentValue.split("\\|").length == 3) return true;
			return false;
		}
	}

	public static class MapFinalClass extends MapReduceBase implements
			Mapper<Text, Text, Text, Text> {

		@Override
		public void map(Text key, Text value,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			String incomingValue = value.toString(); 
			if(isType1Value(incomingValue)) {
				String[] message = incomingValue.split("\\|");
				String nodeLabel = message[0];
				output.collect(new Text(nodeLabel), key);
			}
		}
		
		private boolean isType1Value(String incomingValue) {
			if(incomingValue.split("\\|").length == 2)	return true;
			return false;
		}
	}

	public static class ReduceFinalClass extends MapReduceBase implements
			Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			StringBuffer nodeBuffer = new StringBuffer();
			while (values.hasNext()) {
				String currentValue = values.next().toString();
				nodeBuffer.append(currentValue + ",");
			}

			String communityNodes = nodeBuffer.toString();
			communityNodes = Utility.trimLastCharacter(communityNodes);

			output.collect(new Text(""), new Text(communityNodes));
		}
	}
	/*
	@Override
	public int run(String[] args) throws Exception {

		if(testPropagation(args) ==0) return 0;
		
		Configuration configuration = getConf();
		JobConf job = new JobConf(configuration, MapReduceInitial.class);

		Path in = new Path(args[0]);
		Path out = new Path(args[1]);

		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);

		job.setJobName("MyJob1");
		job.setMapperClass(MapInitialClass.class);
		job.setReducerClass(ReduceInitialClass.class);

		job.setInputFormat(KeyValueTextInputFormat.class);

		//No need as default is tab separated
		//job.set("key.value.separator.in.input.line", "\t");

		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		JobClient.runJob(job);
		return 0;
	}

	private int testPropagation(String[] args) throws Exception{
		Configuration configuration = getConf();
		JobConf job = new JobConf(configuration, MapReduceInitial.class);

		Path in = new Path(args[0]);
		Path out = new Path(args[1]);

		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);

		job.setJobName("MyJob1");
		job.setMapperClass(MapPropagationClass.class);
		job.setReducerClass(ReducePropagationClass.class);
		//job.setReducerClass(IdentityReducer.class);

		job.setInputFormat(KeyValueTextInputFormat.class);

		//No need as default is tab separated
		job.set("key.value.separator.in.input.line", "\t");

		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		JobClient.runJob(job);
		return 0;

		
	}
	*/
	
	@Override
	public int run(String[] args) throws Exception {
		List<Long> changedLabels = runAsSingleJob(args);
		
		String directoryName = args[2];
		FileManager.persistData(directoryName + "/labelChanges.txt", changedLabels);
		return 0;
	}
	
	private List<Long> runAsSingleJob(String[] args) throws Exception{
		String localDirectory = args[2];
		
		List<Long> counters = new ArrayList<Long>();
		
		runInitialJob(args[0], args[1]+"/iteration0");
		String lastIteration = args[1]+"/iteration0";
		String base = args[1]+ "/iteration";
		long labelChanges = 1000;
	//	boolean firstZero = false;
		for(int iteration = 1; iteration < 41; iteration++) {
			labelChanges = runPropagationJob(lastIteration, base+iteration, iteration, counters);
			lastIteration = base+iteration;
			if(labelChanges == 0) {
//				if(firstZero) break;
//				else firstZero = true;
				break;
			}
		}
		DownloadFile.copyFileToLocalSystem(lastIteration + "/part-00000", localDirectory + "/last_iteration.txt");
		
		runFinalJob(lastIteration, args[1]+"/communities");
		DownloadFile.copyFileToLocalSystem(args[1]+"/communities" + "/part-00000", localDirectory + "/communities.txt");
		System.out.println("All Counters --: " + counters);
		return counters;
	}
	
	private int runInitialJob(String input, String output) throws Exception{
		Configuration configuration = getConf();
		JobConf job = new JobConf(configuration, MyMapReduce.class);

		Path in = new Path(input);
		Path out = new Path(output);

		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);

		job.setJobName("Initial Job");
		job.setMapperClass(MapInitialClass.class);
		job.setReducerClass(ReduceInitialClass.class);

		job.setInputFormat(KeyValueTextInputFormat.class);

		//No need as default is tab separated
		//job.set("key.value.separator.in.input.line", "\t");

		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		JobClient.runJob(job);
		
		return 0;
	}
	
	private long runPropagationJob(String input, String output, int iteration, List<Long> counters) throws Exception {
		Configuration configuration = getConf();
		JobConf job = new JobConf(configuration, MyMapReduce.class);

		Path in = new Path(input);
		Path out = new Path(output);

		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);

		job.setJobName("Job for Iteration: " + iteration);
		job.setMapperClass(MapPropagationClass.class);
		job.setReducerClass(ReducePropagationClass.class);
		//job.setReducerClass(IdentityReducer.class);

		job.setInputFormat(KeyValueTextInputFormat.class);

		//No need as default is tab separated
		job.set("key.value.separator.in.input.line", "\t");

		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		RunningJob jobRunning = JobClient.runJob(job);

		long labelChangeCounter = jobRunning.getCounters().findCounter(Constants.LABEL_CHANGES, Constants.LABEL_CHANGES).getValue();
		System.out.println("\n------>>>Counter = " + labelChangeCounter);
		
		long borderLabels = jobRunning.getCounters().findCounter(Constants.NO_VOTE, Constants.NO_VOTE).getValue();
		System.out.println("\n------>>>Border Labels = " + borderLabels);
		
		long reallyBorderNodes = jobRunning.getCounters().findCounter(Constants.REALLY_BORDER_LABELS, Constants.REALLY_BORDER_LABELS).getValue();
		System.out.println("\n------>>>Really Border Labels = " + reallyBorderNodes);
		
		counters.add(labelChangeCounter);
		
		return labelChangeCounter + reallyBorderNodes;
	}

	private int runFinalJob(String input, String output) throws Exception {
		Configuration configuration = getConf();
		JobConf job = new JobConf(configuration, MyMapReduce.class);

		Path in = new Path(input);
		Path out = new Path(output);

		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);

		job.setJobName("Final Job");
		job.setMapperClass(MapFinalClass.class);
		job.setReducerClass(ReduceFinalClass.class);
		//job.setReducerClass(IdentityReducer.class);

		job.setInputFormat(KeyValueTextInputFormat.class);

		//No need as default is tab separated
		job.set("key.value.separator.in.input.line", "\t");

		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.set("mapred.textoutputformat.separator", " ");
		
		JobClient.runJob(job);		
		return 0;
	}
	
	public static void main(String args[]) throws Exception {
		int res = ToolRunner.run(new Configuration(), new MyMapReduce(), args);

		System.exit(res);
	}

}