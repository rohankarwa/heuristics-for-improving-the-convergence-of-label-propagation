package com.community.rohan.mapreduce;

public class LabelHeuristicImpl extends LabelHeuristic{

	
	private String generateLabel(String labels, String badLabels) {
		return labels.split(",")[0];
	}


	public boolean skipTheNode(String labels) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String labelHistoryToPreserve(String currentLabel, String olderLabels) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateLabel(String labels, String badLabels,
			String lastCurrentNodeLabel, int iterationNumber) {
		
		return generateLabel(labels, badLabels);
	}
	
	@Override
	public boolean skipTheNode(String labels, long counter) {
		return skipTheNode(labels);
	}

}
