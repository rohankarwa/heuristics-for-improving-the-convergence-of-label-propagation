package com.community.rohan.mapreduce;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the Heuristics implementation. All the results mentioned in the 
 * project report are generated using this class
 * @author Rohan Karwa
 *
 */
public class LabelHeuristicWindow extends LabelHeuristic{

	private String generateLabel(String labels, String badLabels) {
		List<Integer> listOfBadLabels = generateList(badLabels);
		
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		String[] allLabels = labels.split(",");
		for(String currentLabel : allLabels) {
			String nodeId = currentLabel.substring(1, currentLabel.indexOf(":"));
			if(listOfBadLabels.contains(Integer.parseInt(nodeId.trim()))) 
				continue;
			String nodeLabel = currentLabel.substring(currentLabel.indexOf(":") + 1, currentLabel.length() - 1);
			
			if(countMap.containsKey(nodeLabel)) 
				countMap.put(nodeLabel, countMap.get(nodeLabel) + 1);
			else
				countMap.put(nodeLabel, 1);
		}

		String maxLabel = "";
		List<String> candidateLabels = new ArrayList<String>();
		int maxValue = -1;
		for(String nodeLabel : countMap.keySet()) {
			if(countMap.get(nodeLabel) > maxValue) {
				maxLabel = nodeLabel;
				maxValue = countMap.get(nodeLabel);
				candidateLabels = new ArrayList<String>();
				candidateLabels.add(maxLabel);
			} else if(countMap.get(nodeLabel) == maxValue) {
				candidateLabels.add(nodeLabel);
			}
		}
		
		if(maxLabel.equalsIgnoreCase("")) return maxLabel;
		if(candidateLabels.size() == 1) return maxLabel;
		
		int randomNumber = (int)(Math.random() * candidateLabels.size());
		return candidateLabels.get(randomNumber);
	}

	private List<Integer> generateList(String badLabels) {
		String[] splits = badLabels.split(",");
		List<Integer> listToReturn = new ArrayList<Integer>();
		for(String nodeId : splits) {
			nodeId = nodeId.trim();
			if(nodeId.equalsIgnoreCase("")) continue;
			listToReturn.add(Integer.parseInt(nodeId));
		}
		return listToReturn;
	}


	@Override
	public boolean skipTheNode(String labelList, long counter) {
		String[] labels = labelList.split(",");

		//This is the value of T
		if(labels.length < 21) return false;
//		if(labels.length < 8) 
//			return false;
		String firstLabel = "";
		
		//This is the value for W
		for(int i=0; i<3;i++) {
			if(i == 0) {
				firstLabel = labels[i];
			}
			else {
				if(!firstLabel.equalsIgnoreCase(labels[i])) return true;
			}
		}
		return false;
	}

	@Override
	public String labelHistoryToPreserve(String currentLabel, String olderLabels) {
		return currentLabel + "," + olderLabels;
	}

	@Override
	public String generateLabel(String labels, String badLabels,
			String lastCurrentNodeLabel, int iterationNumber) {
		return generateLabel(labels, badLabels);
	}
	


}