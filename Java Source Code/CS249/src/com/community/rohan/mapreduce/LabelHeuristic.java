package com.community.rohan.mapreduce;

/**
 * This is the abstract class mentioning the contract
 * that all the LabelHeuristics implementation classes should follow
 * Map Reduce program would make reference to this class
 * @author Rohan Karwa
 *
 */
public abstract class LabelHeuristic {

	/**
	 * Generate the label for the node
	 * @param labels	comma separated list for the adjacent node labels
	 * @param badLabels	labels that have been marked for NOT_CONSIDER
	 * @return new label that is to be assigned to the node
	 */
	public abstract String generateLabel(String labels, String badLabels, String lastCurrentNodeLabel, int iterationNumber);
	
	/**
	 * Makes a decision if the message from this node is to be marked as
	 * NOT_CONSIDER
	 * @param labels	history of the labels for this node
	 * @return true if the node is NOT to be considered else returns false
	 */
	public abstract boolean skipTheNode(String labels, long counter);

	/**
	 * Decides how much history for a node is to be maintained. 
	 * @param currentLabel Current Label assignment for this node
	 * @param olderLabels	Older history for the node
	 * @return	the label history that is to be maintained
	 */
	public abstract String labelHistoryToPreserve(String currentLabel, String olderLabels);
	
	/**
	 * 
	 * @return Returns the object of the class that implements this abstract class
	 */
	public static LabelHeuristic getInstance() {
		return new LabelHeuristicSimple();
	}
}
