package com.community.rohan.mapreduce;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

/**
 * Class responsibility is to manage all the File I/O operations
 * @author Rohan Karwa
 *
 */
public class FileManager {

	public static void persistData(String fileName, List<Long> labelsChanges) {
		try{
		    // Create file 
		    FileWriter fstream = new FileWriter(fileName);
		    BufferedWriter out = new BufferedWriter(fstream);
		    for(Long labelChange : labelsChanges) {
		    	out.write("\n" + Long.toString(labelChange));
		    }
		    
		    out.close();
		}catch (Exception e){
		      System.err.println("Error: " + e.getMessage());
		}
	}
//	
//	public static void main(String args[]) {
//		List<Long> list = new ArrayList<Long>();
//		list.add(12l);
//		list.add(22l);
//		FileManager.persistData("/home/hadoop/hadoop/tmp2.txt", list);
//	}
}
